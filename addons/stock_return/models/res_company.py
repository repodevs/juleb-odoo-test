from odoo import api, models, _


class Company(models.Model):
    _inherit = 'res.company'

    @api.model
    def create(self, vals):
        company = super(Company, self).create(vals)
        company.sudo()._create_picking_type_return()
        company.sudo()._generate_picking_type_translations()
        return company

    def _create_picking_type_return(self):
        """ Create StockPickingType `Return` when company created
        """
        self.ensure_one()
        PickingType = self.env['stock.picking.type']

        # get last picking type for this company
        picking_in = PickingType.search_read([('company_id', '=', self.id), ('code', '=', 'incoming'), ('default_location_dest_id', '!=', False)], limit=1, order='sequence desc')
        picking = PickingType.search_read([('company_id', '=', self.id)], limit=1, order='sequence desc')
        code = self.name[:5]
        color = picking and picking[0]['color'] or 0
        last_sequence = picking and picking[0]['sequence'] or 0
        warehouse_id = picking and picking[0].get('warehouse_id', [None])[0] or None
        default_location_dest_id = picking_in and picking_in[0].get('default_location_dest_id', [False])[0] or False

        return_vals = {
                'name': _('Return'),
                'code': 'incoming',
                'use_create_lots': True,
                'use_existing_lots': False,
                'default_location_src_id': False,
                'default_location_dest_id': default_location_dest_id,
                'sequence': last_sequence + 1,
                'barcode': code.replace(" ", "").upper() + "-RETURN",
                'show_reserved': False,
                'sequence_code': 'RT',
                'company_id': self.id,
                'warehouse_id': warehouse_id,
                'color': color,
        }
        return PickingType.create(return_vals)

    def _generate_picking_type_translations(self):
        """ Generate translation for all picking type created from this company
        """
        self.ensure_one()
        PickingType = self.env['stock.picking.type']
        IrTransObj = self.env['ir.translation']
        pickings = PickingType.search([('company_id', '=', self.id)])
        installed_langs = IrTransObj._get_languages()

        baseTr = None
        for picking in pickings:
            # if user not using lang en_US, odoo automaticaly creating translation record
            # then in next step we skip existing translation record
            trOrigin = IrTransObj.search([
                ('name', '=', 'stock.picking.type,name'),
                ('type', '=', 'model'),
                ('res_id', '=', picking.id),
            ])

            if not trOrigin:
                # this maybe user using language en_US when creating this record
                # so we need fetch translation value that created from i18n/LANG_ISO_CODE.po (e.g: id.po)
                baseTr = IrTransObj.search([
                    ('type', '=', 'code'),
                    ('module', 'in', ['stock', 'stock_return']),
                    ('src', '=', picking.name)
                ], limit=1, order='id desc')

            # generate translation for all installed languages
            for iso_code, lang_name in installed_langs:
                if trOrigin:
                    # skip existing translation record
                    if iso_code == trOrigin.lang: # trOrigin.lang is iso_code (e.g: en_US)
                        continue

                new_lang_value = None
                if baseTr:
                    if baseTr.lang == iso_code:
                        new_lang_value = baseTr.value
                    else:
                        new_lang_value = baseTr.src
                else:
                    # get original translation of this lang, then used as new lang value
                    newVal = IrTransObj.search([
                        ('type', '=', 'code'),
                        ('module', 'in', ['stock', 'stock_return']),
                        ('value', '=', picking.name)
                    ], limit=1, order='id desc')
                    new_lang_value = newVal.src

                if new_lang_value and new_lang_value != '':
                    # check existing, if exist skip
                    skip_domain = [
                        ('lang', '=', iso_code),
                        ('res_id', '=', picking.id),
                        ('src', '=', picking.name),
                        ('value', '=', new_lang_value),
                    ]
                    if IrTransObj.search_count(skip_domain) < 1:
                        # create translation
                        IrTransObj.create({
                            'type': 'model',
                            'name': 'stock.picking.type,name',
                            'lang': iso_code,
                            'res_id': picking.id,
                            'src': picking.name,
                            'value': new_lang_value,
                            'state': 'translated',
                        })
