# Juleb Odoo Test

In this task I create module `stock_return` which is used for auto create Stock Picking Type (Operation Type) `Return`
and generate the translation for it when new Company created.

---

## Setup

> Make sure `docker` and `docker-compose` already installed on your machine, and port `8069` is not used.

Run `docker-compose up -d` wait until the service running and up.
> _NOTE: if you are using Apple Silicon (M1), use `docker-compose -f docker-compose.arm64.yml up -d`_ because base docker image odoo currently not support for Apple Silicon.

You also can check the logs using `docker-compose logs -f`

if service already running, open [http://localhost:8069](http://localhost:8069)

<img width="800" alt="Odoo Service up and running" src="https://user-images.githubusercontent.com/13567128/201520020-97c3cc32-25e6-485f-95f2-1a2838e51cde.png">

_Odoo service up and running_

> if you prefer using database backup, please download db backup [odoo13juleb.zip](https://drive.google.com/file/d/192xeU18EbwUgjPbRVXnS7rND6n3SnY6L/view?usp=sharing) and restore in your own Odoo service. the credentials is user:`admin` password:`admin`

---

## Usage

Login to [http://localhost:8069](http://localhost:8069) using credentials user:`admin` and password:`admin`

### Setting Company
Change `Company Name` to `Branch1` (same as Task Requirements Screenshot).

Go To `Settings` > `Users & Companies` > `Companies` > (Select `YourCompany`) >  Edit > Change Name to `Branch1`.

<img width="1190" alt="Change Company Name" src="https://user-images.githubusercontent.com/13567128/201518596-5005d38a-1511-4e35-9109-9b56fe1e60d5.png">

_Change Company Name `YourCompany` to `Branch1`_

### Installing Language
Then Install Language Bahasa Indonesia. (this addons (`stock_return`) for now only translated for Bahasa Indonesa, we can add more language by adding `po` file in directory `stock_return/i18n/LANG_ISO_CODE.po`)

Go To `Settings` > `Translations` > `Languages` > (Search `Bahasa Indonesia`) > Then Activate Language

> make sure Odoo debug mode already activate. _([http://localhost:8069/web?debug=1](http://localhost:8069/web?debug=1))_

<img width="1195" alt="Install Language Bahasa Indonesia" src="https://user-images.githubusercontent.com/13567128/201518603-22d6dce8-89d9-4456-8510-174ecc45ad74.png">

_Install Language `Bahasa Indonesia`_


### Installing `stock_return` module
Go To `Apps` > (Search `stock_return`) > Install

<img width="1193" alt="Install Module stock_return" src="https://user-images.githubusercontent.com/13567128/201518626-0c96e5dc-1ebe-43bf-b333-3495e580ebc1.png">

_Install module `stock_return`_

after `stock_return` installed, you now can create new company and automatically created new Operation Type `Return` and with correct translation when Language changed.

<img width="1193" alt="Create New Company with name Branch2" src="https://user-images.githubusercontent.com/13567128/201518659-8e8e67d5-d3b2-47ce-a87e-425dc14e0c3c.png">

_Create New Company with name `Branch2`_

Reload/Refresh your Browser. then select `Branch2`, you will see new Inventory Operation Type for `Branch2` with name `Receipts`, `Delivery Orders` and `Return`. 

<img width="1196" alt="Inventory Overview" src="https://user-images.githubusercontent.com/13567128/201518677-c31d8d83-7668-4d17-87ad-48a561fa5c2b.png">

_Inventory Overview with `Branch2` selected_

Try change language to `Bahasa Indonesia`

<img width="1190" alt="Change language to Bahasa Indonesia" src="https://user-images.githubusercontent.com/13567128/201519133-37ae052d-a5e2-4440-9ab7-a6a1b2b3b49e.png">


And Inventory Operation Type auto translated to Bahasa Indonesia.

<img width="1193" alt="Inventory Overview with Bahasa Indonesia" src="https://user-images.githubusercontent.com/13567128/201518704-b20c21bf-bddb-477f-ab90-dae26ec6e84a.png">

_Inventory Overview translated to Bahasa Indonesia_